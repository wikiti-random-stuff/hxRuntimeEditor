package hxRuntimeEditor;
import openfl.display.Sprite;
import openfl.events.KeyboardEvent;
import openfl.events.MouseEvent;
import openfl.filters.GlowFilter;
import openfl.geom.Point;
import openfl.geom.Rectangle;
import openfl.Lib;
import openfl.text.TextField;
import openfl.text.TextFieldAutoSize;
import openfl.text.TextFormat;
import openfl.ui.Keyboard;

/**
 * Edit mode for RuntimeEditableSprite
 */
enum EditMode {
  NONE;
  MOVE;
  ROTATE;
  SCALE;
}

 /** Draggable class.
  * Used to allow developers to drag an position sprites in runtime. Not available in release mode.
  * 
  * @author Wikiti */
class RuntimeEditableSprite extends Sprite {
    // -- Constants -- //
  private static inline var TEXT_OFFSET_X: Float = 15.0;
  private static inline var TEXT_OFFSET_Y: Float = 0.0;
  private static inline var ROTATION_SPEED_FACTOR: Float = 0.35;
  private static inline var SCALE_SPEED_FACTOR: Float = 0.01;
  
    // -- Class fields -- //  
  
#if desktop
  /** Action to perform when mouse1 (left) is pressed over the sprite. Used in desktop mode. */
  public static var mouse1_action: EditMode = EditMode.MOVE;
  
  /** Action to perform when mouse2 (...) is pressed over the sprite. Used in desktop mode. */
  public static var mouse2_action: EditMode = EditMode.ROTATE;
  
  /** Action to perform when mouse3 (...) is pressed over the sprite. Used in desktop mode. */
  public static var mouse3_action: EditMode = EditMode.SCALE;
  
  /** Key to enable adjust mode. Used in desktop mode. */
  public static var adjust_key: Int = Keyboard.SHIFT;
  
#end
  
    // -- Object attributes -- */
    
  /** Color to draw sprite bounds, an integer with ARGB format. */
  public var draw_color: Int = 0xFF00FF00;
  
  /** Color for text data, an integer with ARGB format. */
  public var text_color: Int = 0xFFFF0000;
  
  /** Color for outlined text data, an integer with ARGB format. */
  public var text_outline_color: Int = 0xFF000000;
  
  /** Font size of text data. */
  public var text_size: Int = 16;
  
  /** Font type of text data. */
  public var text_font: String = "Consolas";
  
  /** Adjust for positioning (x axis) when pressing the adjust key. */
  public var adjust_positionX: Float = 20.0;
  
  /** Adjust for positioning (y axis) when pressing the adjust key. */
  public var adjust_positionY: Float = 20.0;
  
  /** Adjust for rotating when pressing the adjust key. */
  public var adjust_rotation:    Float = 15.0;
  
  /** Adjust for scale X when pressing the adjust key. */
  public var adjust_scaleX:   Float = 0.5;
  
  /** Adjust for scale Y when pressing the adjust key. */
  public var adjust_scaleY:   Float = 0.5;
    
  private var _current_edit_mode: EditMode = EditMode.NONE;
  private var _text_data: TextField = null;
  private var _adjust_mode: Bool = false;
  private var _last_mouse_pos: Point = null;
  private var _last_scale: Point = null;
  private var _last_rotation: Float = 0.0;
  
    // -- Generic methods -- //
  
  /** Constructor. Same as a Sprite, with some more arguments.
   * @param enabled_edition Start the sprite enabled for editing.
   */
  public function new(start_editable: Bool = true) {
    super();
    
    setEditable( start_editable );
    
#if debug
    _last_mouse_pos = new Point();
    _last_scale = new Point();

    _text_data = new TextField();
    _text_data.filters = [new GlowFilter(text_outline_color, 1.0, 4, 4, 10) ];
    _text_data.selectable = false;
    _text_data.autoSize = TextFieldAutoSize.LEFT;
    _text_data.width = 0;
    _text_data.multiline = true;
#end
  }
  
  /**
   * Set the sprite as editable or not.
   * @param mode Edit mode. true means editable, false means not editable.
   */
  public inline function setEditable(mode: Bool = true) {
#if debug
    if(mode) {
      _add_event_listeners();
    }
    else {
      _remove_event_listeners();
    }
#end
  }
  
   // -- Events -- //
  private inline function _add_event_listeners() {
#if desktop
    this.addEventListener( MouseEvent.MOUSE_DOWN,        _event_mouse1_down );
    this.addEventListener( MouseEvent.RIGHT_MOUSE_DOWN,  _event_mouse2_down );
    this.addEventListener( MouseEvent.MIDDLE_MOUSE_DOWN, _event_mouse3_down );

    Lib.current.stage.addEventListener( MouseEvent.MOUSE_MOVE,      _event_mouse_move );
    Lib.current.stage.addEventListener( MouseEvent.MOUSE_UP,        _unset_edit_mode  );
    Lib.current.stage.addEventListener( MouseEvent.RIGHT_MOUSE_UP,  _unset_edit_mode  );
    Lib.current.stage.addEventListener( MouseEvent.MIDDLE_MOUSE_UP, _unset_edit_mode  );
    
    Lib.current.stage.addEventListener( KeyboardEvent.KEY_DOWN, _key_down );
    Lib.current.stage.addEventListener( KeyboardEvent.KEY_UP,   _key_up   );
#end
  }
  
  private inline function _remove_event_listeners() {
#if desktop

#end
  }
  
#if desktop
  private inline function _event_mouse1_down(event: MouseEvent) {
    // Add text and draw bounds
    _draw_info();
    
    // Setup
    _current_edit_mode = mouse1_action;
  }
  
  private inline function _event_mouse2_down(event: MouseEvent) {
    // Add text and draw bounds
    _draw_info();
    
    // Setup
    _current_edit_mode = mouse2_action;
  }
  
  private inline function _event_mouse3_down(event: MouseEvent) {
    // Add text and draw bounds
    _draw_info();
    
    // Setup
    _current_edit_mode = mouse3_action;
  }
  
  private inline function _event_mouse_move(event: MouseEvent) {
    _run_mouse_action(event);
  }
  
  private inline function _unset_edit_mode(event: MouseEvent) {
    _undraw_info();
    _current_edit_mode = NONE;
  }
  
  private function _run_mouse_action(event: MouseEvent) {
    _text_data.x = event.stageX + TEXT_OFFSET_X;
    _text_data.y = event.stageY + TEXT_OFFSET_Y;
    
    switch(_current_edit_mode) {
      case EditMode.MOVE:
        // Update data
        this.x = event.stageX;
        this.y = event.stageY;
        
        if (_adjust_mode) {
          this.x = this.x - this.x % adjust_positionX;
          this.y = this.y - this.y % adjust_positionY;
        }
        
        // Update text
        _update_text();
        
      case EditMode.ROTATE:
          
        // Rotate with X axis.          
        if (_adjust_mode) {
          _last_rotation += (event.stageX - _last_mouse_pos.x) * ROTATION_SPEED_FACTOR;
          this.rotation = _last_rotation - _last_rotation % adjust_rotation;
        }
        else {
          this.rotation += (event.stageX - _last_mouse_pos.x) * ROTATION_SPEED_FACTOR;
          _last_rotation = this.rotation;
        }
        
        // Update text
        _update_text();
        
      case EditMode.SCALE:

        // Scale x with mouse x axis and scale y with mouse y axis
        if (_adjust_mode) {
          _last_scale.x += (event.stageX - _last_mouse_pos.x) * SCALE_SPEED_FACTOR;
          _last_scale.y -= (event.stageY - _last_mouse_pos.y) * SCALE_SPEED_FACTOR;
          this.scaleX = _last_scale.x - _last_scale.x % adjust_scaleX;
          this.scaleY = _last_scale.y - _last_scale.y % adjust_scaleY;
        }
        else {
          this.scaleX += (event.stageX - _last_mouse_pos.x) * SCALE_SPEED_FACTOR;
          this.scaleY -= (event.stageY - _last_mouse_pos.y) * SCALE_SPEED_FACTOR;

          _last_scale.x = this.scaleX;
          _last_scale.y = this.scaleY;
        }
        
        // Update text
        _update_text();
        
	    default:

    }

	  _last_mouse_pos.x = event.stageX;
	  _last_mouse_pos.y = event.stageY;
  }
  
  private inline function _key_down (event: KeyboardEvent) {
    if (event.keyCode == adjust_key)
      _adjust_mode = true;
  }
  
  private inline function _key_up (event: KeyboardEvent) {
    if (event.keyCode == adjust_key)
      _adjust_mode = false;
  }

#end

  private function _draw_info() {
    Lib.current.stage.addChild(_text_data);
        
    this.graphics.clear();
    this.graphics.lineStyle(2.0, text_color, 1.0);
    var r: Rectangle = this.getBounds(this);
    this.graphics.drawRect(r.x, r.y, r.width, r.height);
  }
  
  private function _undraw_info() {
    Lib.current.stage.removeChild(_text_data);
    
    this.graphics.clear();
  }
  
  private function _update_text() {
    _text_data.defaultTextFormat = new TextFormat( text_font, text_size, text_color );

    _text_data.text = this.toString() + "\n";
    _text_data.text += "Pos X:   " + round(this.x) + "\n";
    _text_data.text += "Pos Y:   " + round(this.y) + "\n";
    _text_data.text += "Angle:   " + round(this.rotation) + "\n";
    _text_data.text += "Scale X: " + round(this.scaleX) + "\n";
    _text_data.text += "Scale Y: " + round(this.scaleY) + "\n";
    _text_data.text += "Width:   " + round(this.width) + "\n";
    _text_data.text += "Height:  " + round(this.height);
	
  }
  
  private static inline function round(number:Float, ?precision=2): Float {
    number *= Math.pow(10, precision);
    return Math.round(number) / Math.pow(10, precision);
  }

}