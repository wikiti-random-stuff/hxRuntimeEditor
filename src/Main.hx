package ;

import openfl.display.Sprite;
import openfl.events.Event;
import openfl.Lib;
import openfl.Assets;
import openfl.display.Bitmap;

import hxRuntimeEditor.RuntimeEditableSprite;

/**
 * ...
 * @author Daniel Herzog
 */

 private class Sheet extends RuntimeEditableSprite {
   public function new() {
     super();
     
     var bitmap: Bitmap = new Bitmap( Assets.getBitmapData ( "img/test.png" ) );
     bitmap.x = - bitmap.width  / 2.0;
     bitmap.y = - bitmap.height / 2.0;
     this.addChild(bitmap);
     
     this.x = 320;
     this.y = 120;
     
     this.rotation = -39.3;
     
     this.scaleX = 0.93;
     this.scaleY = 1.25;
   }
   
   
 }

class Main extends Sprite 
{
	var inited:Bool;

	/* ENTRY POINT */
	
	function resize(e) 
	{
		if (!inited) init();
		// else (resize or orientation change)
	}
	
	function init() 
	{
		if (inited) return;
		inited = true;
    
    var sprite: Sprite = new Sprite();
    sprite.addChild( new Bitmap( Assets.getBitmapData ( "img/front.png" ) ) );
    
    this.addChild(sprite);

    
    this.addChild( new Sheet() );
		// (your code here)
		
		// Stage:
		// stage.stageWidth x stage.stageHeight @ stage.dpiScale
		
		// Assets:
		// nme.Assets.getBitmapData("img/assetname.jpg");
	}

	/* SETUP */

	public function new() 
	{
		super();	
		addEventListener(Event.ADDED_TO_STAGE, added);
	}

	function added(e) 
	{
		removeEventListener(Event.ADDED_TO_STAGE, added);
		stage.addEventListener(Event.RESIZE, resize);
		#if ios
		haxe.Timer.delay(init, 100); // iOS 6
		#else
		init();
		#end
	}
	
	public static function main() 
	{
    
    #if debug
      trace("debug");
    #end
    
		// static entry point
		Lib.current.stage.align = flash.display.StageAlign.TOP_LEFT;
		Lib.current.stage.scaleMode = flash.display.StageScaleMode.NO_SCALE;
		Lib.current.addChild(new Main());
	}
}
