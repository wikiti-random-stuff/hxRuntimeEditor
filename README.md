# hxRuntimeEditor #

## 1. Description ##

An openfl library used to modify *Sprite*'s position, scale and rotation in runtime, used to adjust sprite's in action.


## 2. Installation  ##

Just run in a console

	$ haxelib install hxRuntimeEditor


to install the library and all its dependencies, and add it to your project *xml* file like this:

    <haxelib name="hxRuntimeEditor" />

## 3. Usage ##

Instead of creating and instance of the *Sprite* class, use *RuntimeEditableSprite*:

	private class TestClass extends RuntimeEditableSprite {
	  public function new() {
	    super();
	
	    // Code stuff here
	    // ...
	  }
	}

Remember to compile in **Debug** mode in [FlashDevelop](http://www.flashdevelop.org) to use it, or add the option to the *haxe* compiler with the option *-Ddebug*.


Controls listed down here:

- Press *Mouse1* (left mouse button) to move the *RuntimeEditableSprite*.
- Press *Mouse2* (right mouse button) to rotate the *RuntimeEditableSprite*, and move only the **X** axis of the mouse.
- Press *Mouse3* (middle mouse button) to scale the *RuntimeEditableSprite*: mouse **X** changes the **scaleX** property, and mouse **Y** changes the **scaleY** property.
- Press *Shift* to enable adjusting mode, so it can be adjusted to a simple grid.

All parameters can de adjusted in the *RuntimeEditableSprite* object fields:

	  var test: RuntimeEditableSprite  = new RuntimeEditableSprite();

	  /** Color to draw sprite bounds, an integer with ARGB format. */
	  test.draw_color = 0xFF00FF00;
	  
	  /** Color for text data, an integer with ARGB format. */
	  test.text_color: Int = 0xFFFF0000;
	  
	  /** Color for outlined text data, an integer with ARGB format. */
	  test.text_outline_color = 0xFF000000;
	  
	  /** Font size of text data. */
	  test.text_size = 16;
	  
	  /** Font type of text data ("_sans", "_serif", "Arial", etc). */
	  test.text_font = "Consolas";
	  
	  /** Adjust for positioning (x axis) when pressing the adjust key. */
	  test.adjust_positionX = 20.0;
	  
	  /** Adjust for positioning (y axis) when pressing the adjust key. */
	  test.adjust_positionY = 20.0;
	  
	  /** Adjust for rotating when pressing the adjust key. */
	  test.adjust_rotation = 15.0;
	  
	  /** Adjust for scale X when pressing the adjust key. */
	  test.adjust_scaleX = 0.5;
	  
	  /** Adjust for scale Y when pressing the adjust key. */
	  test.adjust_scaleY = 0.5;


## 4. Release notes ##

- **version 1.0.1** -- 1 Feb 2015 
	- Changed folder hierarchy.
- **version 1.0.0** -- 1 Feb 2015 
	- Released.


## 4. Authors ##

This project has been made by:

| Avatar  | Name          | Nickname  | Email              |
| ------- | ------------- | --------- | ------------------ |
| ![](http://i59.tinypic.com/fasosx.jpg)  | Daniel Herzog | Wikiti | [wikiti.doghound@gmail.com](mailto:wikiti.doghound@gmail.com)

License
=======
This software is under the WTFPL License:

	DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
	                    Version 2, December 2004
	
	 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
	
	 Everyone is permitted to copy and distribute verbatim or modified
	 copies of this license document, and changing it is allowed as long
	 as the name is changed.
	
	            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
	   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
	
	  0. You just DO WHAT THE FUCK YOU WANT TO.